#
# Load extensions
load('ext://namespace', 'namespace_create', 'namespace_inject')

#
# Variables
helm_values  = []
hostname     = 'pmapp.test'
namespace    = os.environ.get('DEPLOYED_NAMESPACE', None)
namespaces   = str(local("kubectl get ns -o=jsonpath='{range .items[*]}{.metadata.name}{\"\\n\"}{end}'", quiet=True, echo_off=True)).splitlines()
environment  = os.environ.get("DEPLOYED_ENV", "dev") # dev, staging
build_target = 'production' if environment != 'dev' else 'develop'
commit_ids   = str(local("git log -4 --pretty=format:'%h' --abbrev=7", quiet=True, echo_off=True)).splitlines()

#
# Determine namespace: Environment variable takes presedence.
if namespace:
    ns = namespace

elif os.environ.get('CI_COMMIT_REF_SLUG') and os.environ.get('CI_PIPELINE_ID'):
    ns = '%s-%s' % (os.environ.get('CI_COMMIT_REF_SLUG'), os.environ.get('CI_PIPELINE_ID'))

else:
    git_branch=str(local('git symbolic-ref --short -q HEAD', quiet=True, echo_off=True)).rstrip('\n')

    if git_branch:
        ns = git_branch.lower()
    else:
        ns = 'user-%s' % os.environ.get('USER', 'anonymous')

if len(ns) > 63:
    ns = ns[0:63]

if not ns in namespaces:
    namespace_create(ns)

#
# Determine if a helm dependency update is required.
if config.tilt_subcommand == "up" or config.tilt_subcommand == "ci":
    update_deps = False

    app_chart = read_yaml('charts/testapp/Chart.yaml')
    for dep in app_chart['dependencies']:
        if not os.path.exists('charts/testapp/charts/' + dep['name'] + '-' + dep['version'] + '.tgz'):
            update_deps = True
            break

    if update_deps:
        local('helm dep update charts/testapp')

k8s_resource( workload='pm-testapp', port_forwards=['5000:5000'], links=[link('https://' + hostname)])

k8s_yaml(
    helm(
        'charts/testapp',
        values = 'charts/values.'+ environment +'.yaml',
        set = helm_values,
        namespace = ns
    )
)

docker_build(
    'pm-testapp', 'src/',
    build_args={'BUILDKIT_INLINE_CACHE': '1'},
    dockerfile='src/Dockerfile',
    target=build_target,
    extra_tag='pm-testapp:' + commit_ids[0],
    cache_from=['pm-testapp:' + id for id in commit_ids],
    live_update=[
        fall_back_on([
            'src/requirements.txt'
        ]),
        sync('src', '/opt/app-root/src'),
    ] if build_target == 'develop' else []
)