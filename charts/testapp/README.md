# testapp

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 0.1.0](https://img.shields.io/badge/AppVersion-0.1.0-informational?style=flat-square)

Our awesome test app

## Requirements

| Repository | Name | Version |
|------------|------|---------|
| file://../redis | redis | 0.1.0 |

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` |  |
| autoscaling.enabled | bool | `false` |  |
| autoscaling.maxReplicas | int | `100` |  |
| autoscaling.minReplicas | int | `1` |  |
| autoscaling.targetCPUUtilizationPercentage | int | `80` |  |
| fullnameOverride | string | `""` | String to fully override testapp.fullname template |
| image.pullPolicy | string | `"IfNotPresent"` |  |
| image.registry | string | `""` | Override the default registry |
| image.repository | string | `"pm-testapp"` | Container image repository |
| image.tag | string | the chart appVersion | Overrides the image tag |
| imagePullSecrets | list | `[]` | Optionally specify an array of imagePullSecrets (secrets must be manually created in the namespace) |
| ingress.annotations | object | `{}` |  |
| ingress.enabled | bool | `false` |  |
| ingress.hosts | list | `[]` |  |
| ingress.tls | list | `[]` |  |
| nameOverride | string | `""` | String to partially override testapp.name template (will maintain the release name) |
| nodeSelector | object | `{}` |  |
| podAnnotations | object | `{}` |  |
| podDisruptionBudget.enabled | bool | `false` |  |
| podDisruptionBudget.minAvailable | int | `1` |  |
| podSecurityContext | object | `{}` |  |
| redis.enabled | bool | `true` |  |
| redisHost | string | `""` | String for Redis hostname |
| redisSecret | string | `""` | Secret name containing redis connection string (mutually exclusive with redisHost) |
| replicaCount | int | `1` | Number of replicas |
| resources | object | `{}` |  |
| securityContext | object | `{}` |  |
| service.port | int | `5000` |  |
| service.type | string | `"ClusterIP"` |  |
| serviceAccount.annotations | object | `{}` | Annotations to add to the service account |
| serviceAccount.create | bool | `true` | Specifies whether a service account should be created |
| serviceAccount.name | string | `""` | The name of the service account to use. If not set and create is true, a name is generated using the fullname template |
| tolerations | list | `[]` |  |
