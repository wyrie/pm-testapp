# redis

![Version: 0.1.0](https://img.shields.io/badge/Version-0.1.0-informational?style=flat-square) ![Type: application](https://img.shields.io/badge/Type-application-informational?style=flat-square) ![AppVersion: 6.2.4](https://img.shields.io/badge/AppVersion-6.2.4-informational?style=flat-square)

Our customer redis chart

## Values

| Key | Type | Default | Description |
|-----|------|---------|-------------|
| affinity | object | `{}` |  |
| fullnameOverride | string | `""` |  |
| image.pullPolicy | string | `"IfNotPresent"` |  |
| image.registry | string | `""` | Override the default registry |
| image.repository | string | `"redis"` | Container image repository |
| image.tag | string | the chart appVersion | Overrides the image tag |
| imagePullSecrets | list | `[]` |  |
| nameOverride | string | `""` |  |
| nodeSelector | object | `{}` |  |
| persistence.accessModes | list | `["ReadWriteOnce"]` | Persistent Volume access modes |
| persistence.annotations | object | `{}` | Additional custom annotations for the PVC |
| persistence.enabled | bool | `true` | Specifies whether a persistent volume claim should be created |
| persistence.path | string | `"/data"` | The path the volume will be mounted at on the redis container |
| persistence.size | string | `"8Gi"` | Persistent Volume size |
| persistence.storageClass | string | `""` | Persistent Volume storage class |
| podAnnotations | object | `{}` |  |
| podSecurityContext | object | `{}` |  |
| replicaCount | int | `1` | Number of replicas |
| resources | object | `{}` |  |
| securityContext | object | `{}` |  |
| service.port | int | `6379` |  |
| service.type | string | `"ClusterIP"` |  |
| serviceAccount.annotations | object | `{}` | Annotations to add to the service account |
| serviceAccount.create | bool | `true` | Specifies whether a service account should be created |
| serviceAccount.name | string | `""` | The name of the service account to use. If not set and create is true, a name is generated using the fullname template |
| tolerations | list | `[]` |  |
