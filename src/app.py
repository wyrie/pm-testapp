from flask import Flask, current_app
from flask.json import dumps
import redis
import redis.exceptions
import socket
import os

hostname = socket.gethostname()

# reads HOST from REDIS_HOST env variable. Defaults to "redis" if not set.
REDIS_HOST = os.getenv('REDIS_HOST', "redis")

app = Flask(__name__)
_redis = redis.Redis(REDIS_HOST)

@app.route("/")
def root():
    _redis.incr("visited")
    c = int(_redis.get("visited"))
    return f"hello! My hostname is {hostname} and this site was visited {c} times!"


@app.route("/healthz")
def healthz():
    """Endpoint to check flask is serving requests and can connect to redis"""

    try:
        _redis.ping()

        status = 200
        title = "OK"

    except (redis.exceptions.ConnectionError,
            redis.exceptions.TimeoutError) as conn_error:
        status = 503
        title = str(conn_error)

    return current_app.response_class(
        dumps({"status": status, "title": title}),
        status=status,
        content_type="application/problem+json",
    )


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)
